package homework_0928;

public class FinallyDemo {
    public static void main(String[] args) {
        try {
            String line = null;
            System.out.println(line.length());
        } catch (Exception e) {
            System.out.println("程序出错了");
        } finally {
            System.out.println("finally中的语句执行了");
        }
    }
}
