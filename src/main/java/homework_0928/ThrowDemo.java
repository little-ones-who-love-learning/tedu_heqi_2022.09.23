package homework_0928;

public class ThrowDemo {
    public static void main(String[] args) {
        Person p = new Person();
        try {
            p.setAge(105);
        } catch (IllegalAgeExceeption e) {
            e.printStackTrace();
        }
        System.out.println("此人年龄为"+p.getAge());
    }
}
