package homework_0928;

public class Person {
    private int age;

    public int getAge() {
        return age;
    }

    public void setAge(int age) throws IllegalAgeExceeption {
        if(age<0||age>100){
            //方法内抛出自定义异常,方法的声明上也应该声明异常,如果忘记可以回忆这个例子
            throw new IllegalAgeExceeption("年龄不合法");
        }
        this.age = age;
    }
}
