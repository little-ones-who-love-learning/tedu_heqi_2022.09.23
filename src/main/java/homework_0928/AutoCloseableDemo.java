package homework_0928;

import java.io.FileOutputStream;
import java.io.IOException;

public class AutoCloseableDemo {
    public static void main(String[] args) {
        /**
         * 所有的流都实现了AutoCloseable接口,在try之后加上(),将流的创建过程写入
         * 最后不需要关闭流,流可以自动关闭
         */
        try(FileOutputStream fos = new FileOutputStream("fos.dat");) {
            fos.write(5);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
