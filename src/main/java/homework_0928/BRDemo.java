package homework_0928;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class BRDemo {
    public static void main(String[] args) throws IOException {
        FileInputStream fis = new FileInputStream("diary.txt");
        InputStreamReader isr = new InputStreamReader(fis);
        BufferedReader br = new BufferedReader(isr);
        String line;
        /**
         *readLine()是BufferedReader提供的方法,读取文件中的一行字符串,
         * 若读到了文件末尾,则返回空字符串
         */
        while(!(line = br.readLine()).isEmpty()){
            System.out.println(line);
        }
    }
}
