package homework_0928;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class PWDemo2 {
    public static void main(String[] args) throws FileNotFoundException {
        /**
         * 使用流连接完成PW的创建
         * 过程PrintWriter->BufferedWriter->OutputStream->FileOutputStream
         *按行写出字符串
         * 加速块写文本数据
         * 将字符转换为字节
         * 将字节数据写入文件中
         */
        FileOutputStream fos = new FileOutputStream("diary.txt",true);
        OutputStreamWriter osw = new OutputStreamWriter(fos,StandardCharsets.UTF_8);
        BufferedWriter bos = new BufferedWriter(osw);
        PrintWriter pw = new PrintWriter(bos);
        Scanner scan = new Scanner(System.in);
        System.out.println("请写入想输入的内容");
        while(true){
            String line = scan.nextLine();
            if(line.equalsIgnoreCase("exit")){
                break;
            }
            pw.write(line);
        }
        pw.close();
    }
}
