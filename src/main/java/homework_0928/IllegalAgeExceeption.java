package homework_0928;

/**
 * 自定义的异常要继承Exception
 */
public class IllegalAgeExceeption extends Exception{
    public IllegalAgeExceeption() {
    }

    public IllegalAgeExceeption(String message) {
        super(message);
    }

    public IllegalAgeExceeption(String message, Throwable cause) {
        super(message, cause);
    }

    public IllegalAgeExceeption(Throwable cause) {
        super(cause);
    }

    public IllegalAgeExceeption(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
