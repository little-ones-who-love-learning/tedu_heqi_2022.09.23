package homework_0928;

import java.awt.*;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ThrowsDemo {
    public void dosome() throws IOException, AWTException {

    }
}

class Subclass extends ThrowsDemo {
    //    @Override
//    抛出全部异常
//    public void dosome() throws IOException, AWTException {
//
//    }
//    抛出部分异常
//    public void dosome() throws IOException {
//
//    }
//    不抛出异常
//    public void dosome(){
//
//    }
//    抛出子类型异常
//    public void dosome() throws FileNotFoundException {
//
//    }

//    不允许抛出超类异常,
//    不允许抛出额外异常,
}
