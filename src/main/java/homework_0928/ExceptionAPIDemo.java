package homework_0928;

public class ExceptionAPIDemo {
    public static void main(String[] args) {
        System.out.println("程序开始了");
        String str = "abc";
        try {
            System.out.println(Integer.parseInt(str));
        } catch (NumberFormatException e) {
            System.out.println("程序出错了");
            //输出错误的堆栈信息,便于定位错误
            e.printStackTrace();
            //输出错误信息
            String message = e.getMessage();
            System.out.println(message);
        }
        System.out.println("程序结束了");
    }
}
