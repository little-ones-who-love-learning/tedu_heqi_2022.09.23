package homework_0928;

public class FinallyDemo2 {
    public static void main(String[] args) {
        //分别说明final,finally,finalize是什么
        //finalize是Object定义的一个方法,该方法是一个对象声明周期的最后一个方法.被GC调用
        //当GC 对象发现一个对象没有引用后,就会将其释放,释放前调用的最后一个方法就是finalize
        System.out.println(test("0")+","+test(null)+","+test(""));
    }

    private static int test(String s) {
        try {
            return s.charAt(0)-'0';
        } catch (NullPointerException e) {
            return 1;
        } catch(Exception e){
            return 2;
        }finally {
            return 3;//finally 一般不会有return,否则一定返回这个值
        }

    }
}
