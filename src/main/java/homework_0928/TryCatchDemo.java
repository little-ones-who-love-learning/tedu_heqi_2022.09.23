package homework_0928;

/**
 * java异常处理机制
 * java.lang.Throwable是所有异常的超类,下面派生了两个子类型
 * Exception :是可恢复的异常,
 * Error:系统错误,不可恢复.
 * 异常处理机制,通常不关心error
 */
public class TryCatchDemo {
    public static void main(String[] args) {
        System.out.println("程序开始了");
//        String line = null;
//        String line = "";
        try {
            String line = "a";
            System.out.println(line.length());
            System.out.println(line.charAt(0));
            System.out.println(Integer.parseInt(line));
            System.out.println(Integer.parseInt(line));
            //代码出错的话,之后的片段都不会再执行
            System.out.println("!!!!!!!!!!!!");
        } catch (NullPointerException |StringIndexOutOfBoundsException e) {
            System.out.println("出现了空指针异常或下表越界异常并解决了");;
        } catch (Exception e){
            System.out.println("其他错误");
        }
        System.out.println("程序结束了");
    }
}
