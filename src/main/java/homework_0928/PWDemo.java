package homework_0928;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class PWDemo {
    public static void main(String[] args) throws FileNotFoundException {
        /**
         * 缓冲字符流java.io.BufferedWriter,java.io.BufferedReader
         * PrintWriter是具有自动行刷新的缓冲字符输出流,内部总是连接BufferedWriter
         *可以按行写出字符串
         */
        PrintWriter pw = new PrintWriter("fos.txt");
        pw.write("让我再看你一遍,从南到北");
        pw.write("像是北五环路,蒙蔽的双眼");
        pw.close();

    }
}
