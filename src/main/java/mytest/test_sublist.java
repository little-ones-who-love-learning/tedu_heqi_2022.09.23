package mytest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class test_sublist {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("one");
        list.add("two");
        list.add("three");
        list.add("four");
        list.add("five");
        System.out.println(list);
        Collections.reverse(list);
        System.out.println(list);
        List<String> list1 = list.subList(1,3);
        System.out.println("list1:"+list1);
        list1.set(0,"three");
        System.out.println("list:"+list);
        list.set(1,"four");
        list1.set(0,"zero");
        System.out.println("list:"+list);
        System.out.println("list1:"+list1);

        Collections.sort(list);
        System.out.println(list);
        /*在sort()后的参数中，第一个参数传入集合，第二个是内部类形式的比较器
        * 这就是自定义比较器
        * 实际开发中，我们对自己的类不会实现Comparable接口，因为存在侵入性*/
        list.sort((o1, o2) -> o1.length() - o2.length());
        System.out.println(list);
    }
}
