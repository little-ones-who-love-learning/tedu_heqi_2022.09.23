package mytest;

import java.util.*;

public class test_map_key {
    public static void main(String[] args) {
        /*map数组里key是唯一的
        * 添加同样的key的话,会替代*/
        Map map= new HashMap();
        String line = "code";
        map.put(line,5);
        map.put(line,6);
        System.out.println(map.size());
        /*遍历map数组的三个方式
        * map是个接口,有以下方法
        * .size()
        * .isEmpty()
        * .containsKey(Object key)
        * .containsValue(Object value)
        * .get(Object value)
        * put(key,value)
        * remove(Object key)
        * .clear()
        * values()用来遍历所有的values
        * Set<key> keySet()
        * Collections<V> values()
        * Set<map.entry<K,V>>  .entrySet()*/
        map.put("语文",66);
        map.put("数学",66);
        map.put("英语",66);
        Set<String> set = map.keySet();
        for(String s:set){
            System.out.println(s);
        }
        /*values()的返回值是一个Collection 类型的集合*/
        Collection collection = map.values();
        System.out.println(collection);

    }
}
