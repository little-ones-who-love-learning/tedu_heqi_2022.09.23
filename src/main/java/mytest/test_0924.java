package mytest;

import java.util.ArrayList;
import java.util.Iterator;

public class test_0924 {
    public static void main(String[] args) {
        /*集合只能存放引用类型的数据,不能存放基本类型的数据
        * byte,short,int,long,boolean,float,double,char
        * Integer是int的包装类,所以是引用类型,使用前需实例化才能引用
        * Integer包括了很多方法,
        * 在Integer类中, == 表示的是地址是否相同
        * 同时,Integer类重写了 .equals()方法 参数为Integer对象,判断值是否相等
        * 如果参数为一个int型,会自动装箱为Integer型
        * 自动拆箱功能a == b a为一个int,b为一个Integer型,b自动拆箱为int型,两者比较值*/
        /*非new生成的Integer对象与new生成的对象在比较时,结果为false
        * 前者指向常量池,后者指向堆*/
        Integer i = new Integer(5);
        Integer j = 6 ;
        System.out.println(i == j);//false
        /*对于两个非new生成的对象,若进行==比较
        * 若两个都在-128到127之间,比较的是值的大小*/
        Integer k = -12;
        Integer l = -12;
        System.out.println(k == l);
        ArrayList<Integer> list = new ArrayList<>();
        /*list.add()集合的添加元素的方法,自带下标了*/
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        /*下面程序会犯错,在迭代器迭代过程中,对集合元素不能增删
        * ConcurrentModificationException
        *list.iterator(),list对象的迭代器生成方式
        * iterator.hasNext()顾名思义,是否有下一个值
        * iterator.next()返回下个对象的值*/
        Iterator<Integer> iterator = list.iterator();
        while(iterator.hasNext()){
            Integer it = iterator.next();
            if(it == 2 ){
                list.remove(it);
            }
        }
    }
}
