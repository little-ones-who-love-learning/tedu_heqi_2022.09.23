package mytest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class test_toArray {
    public static void main(String[] args) {
        Collection<String> li = new ArrayList<>();
        li.add("one");
        li.add("two");
        li.add("three");
        li.add("four");
        /*这里的参数仍有待理解*/
        String[] array =li.toArray(new String[0]);
        /*toString()是数组的工具类Arrays提供的一个静态方法*/
        System.out.println(Arrays.toString(array));
    }

}
