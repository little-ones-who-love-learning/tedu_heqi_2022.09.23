package mytest;

import java.util.ArrayList;
import java.util.List;

public class test_new_again {
    public static void main(String[] args) {
        List l = new ArrayList();
        l.add(1);
        print(l);
        System.out.println(l.get(1));
    }
    static void print(List list){
        list.add(2);
        /*重新new对象,对象的地址更换,其他未发生变化*/
        list = new ArrayList();
        /*之后的操作不再影响原集合中的list*/
        list.add(3);
        list.add(4);

    }
}
