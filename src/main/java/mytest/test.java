package mytest;

import java.util.ArrayList;
import java.util.List;

public class test {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        for(int i=0;i<10;i++){
            list.add(i*10); //自动装箱
        }
        System.out.println(list); //[0, 10, 20, 30, 40, 50, 60, 70, 80, 90]

        //获取下标3到7的子集
        List<Integer> subList = list.subList(3,8);
        System.out.println(subList); //[30, 40, 50, 60, 70]

        //将子集每个元素都扩大10倍
        for(int i=0;i<subList.size();i++){
            subList.set(i,subList.get(i)*10);
        }
        System.out.println(subList); //[300, 400, 500, 600, 700]

        //注意:对子集的操作就是对原集合对应元素操作
        System.out.println(list); //[0, 10, 20, 300, 400, 500, 600, 700, 80, 90]

        list.remove(5);
        System.out.println(list); //[10, 20, 300, 400, 500, 600, 700, 80, 90]
        subList.set(0,20);
        //原集合修改之后，子集将不能再进行操作了，操作则发生异常，但是可以重新获取子集
        //System.out.println(subList); //发生不支持修改异常

    }
}
