package mytest;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class test_runnable implements Runnable{
    /*如果类实现了Runnable接口,是必需要重写run()方法的*/
    public static void main(String[] args) throws FileNotFoundException {
        /*流这里必须抛出异常 throws FileNotFoundException
        * 参数内部传入了文件地址，文件找不到就会报异常*/
        FileInputStream f = new FileInputStream(".");
        int d = 0;
        try {
            /*流的读写操作,要处理IOException
            * try()catch{}*/
            d = f.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(d);

    }

    @Override
    /*重写的run()方法不能使用throws抛出任何异常*/
    public void run() {

    }
}
