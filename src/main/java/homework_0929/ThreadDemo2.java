package homework_0929;

/**
 * 第二种创建线程的方式:实现Runnable接口单独定义线程任务
 */
public class ThreadDemo2 {
    public static void main(String[] args) {
        Thread  t1 = new MyThread1();
        Thread  t2 = new MyThread2();
        t1.start();
        t2.start();
    }
}
class MyRunnable1 implements Runnable{
    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            System.out.println("你谁啊你");
        }
    }
}
class MyRunnable2 implements Runnable{
    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            System.out.println("我是你爹");
        }
    }
}
