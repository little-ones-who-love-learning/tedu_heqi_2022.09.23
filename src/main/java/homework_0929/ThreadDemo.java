package homework_0929;

/**
 * 线程的第一种创建方式
 * 1:继承Thread
 * 2:重写run()方法,定义线程执行的任务代码
 * 3:调用线程的start方法将线程启动
 */
public class ThreadDemo {
    public static void main(String[] args) {
        Thread t1 = new MyThread1();
        Thread t2 = new MyThread2();
        t1.start();
        t2.start();
    }
}
class MyThread1 extends Thread{
    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            System.out.println("你谁呀你");
        }
    }
}
class MyThread2 extends Thread{
    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            System.out.println("我是你爹");
        }
    }
}
