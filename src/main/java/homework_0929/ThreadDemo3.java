package homework_0929;

/**
 * 用匿名内部类的方式完成线程的两种创建方式
 */
public class ThreadDemo3 {
    public static void main(String[] args) {
        Thread t1 = new Thread() {
            @Override
            public void run() {
                for (int i = 0; i < 1000; i++) {
                    System.out.println("你谁呀你");
                }
            }

        };
        Thread t2 = new Thread(() -> {
            for (int i = 0; i < 1000; i++) {
                System.out.println("我是查水表的");
            }
        });
        t1.start();
        t2.start();
    }
}
