package homework_0929;

/**
 * java中所有的代码都是靠线程运行的,main方法也不例外,执行main方法的线程为主线程
 * Thread提供了一个静态方法:
 * static Thread currentThread()
 * 该方法可以获取运行这个方法的线程
 */
public class CurrentThreadDemo {
    public static void main(String[] args) {
        Thread main = Thread.currentThread();
        System.out.println("主线程:"+main);
        dosome();//主线程执行到这里就调用了dosome方法
    }
    public static void dosome(){
        Thread t = Thread.currentThread();
        System.out.println("执行本方法的线程为"+t);
    }
}
