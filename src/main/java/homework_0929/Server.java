package homework_0929;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class Server {
    private ServerSocket serverSocket;
    //该集合用于存放所有的输出流
    private List<PrintWriter> allOut = new ArrayList<>();

    public Server() {
        try {
            System.out.println("正在启动服务端");
            serverSocket = new ServerSocket(8088);
            System.out.println("服务端启动完毕");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void start() {
        try {
            while (true) {
                System.out.println("等待客户端连接");
                Socket socket = serverSocket.accept();
                System.out.println("一个客户端连接了");
                ClientHandler handler = new ClientHandler(socket);
                Thread thread = new Thread(handler);
                thread.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {
        Server server = new Server();
        server.start();
    }
    private class ClientHandler implements Runnable {
        private Socket socket;
        private String host; //记录客户端的IP地址信息
        @Override
        public void run() {
            PrintWriter pw = null;
            try {
                //创建输入流
                InputStream is = socket.getInputStream();
                InputStreamReader isr = new InputStreamReader(is, StandardCharsets.UTF_8);
                BufferedReader br = new BufferedReader(isr);
                //创建输出流
                OutputStream os = socket.getOutputStream();
                OutputStreamWriter osw = new OutputStreamWriter(os, StandardCharsets.UTF_8);
                pw = new PrintWriter(osw);
                //将创建的输出流存入共享集合allOut
                synchronized (allOut){
                    allOut.add(pw) ;
                }
                System.out.println(host + "上线了,当前在线人数:" + allOut.size());
                String message;
                //循环读取客户端发来的消息,不是null就在控制台上输出
                while ((message = br.readLine()) != null) {
                    System.out.println(host + "说:" + message);
                    //将消息回复给所有客户端,相当于这是群聊界面
                    synchronized (allOut) {
                        for (PrintWriter p : allOut) {
                            p.println(host + "说:" + message);
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                synchronized (allOut){
                    allOut.remove(pw);
                }
                System.out.println(host+"下线了,当前在线人数:"+allOut.size());
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        public ClientHandler(Socket socket) {
            this.socket = socket;
            //获取客户端的IP地址
            host = socket.getInetAddress().getHostAddress();
        }
    }
}

