package homework_0929;

/**
 * 获取线程信息的相关方法
 */
public class ThreadInfoDemo {
    public static void main(String[] args) {
        Thread main = Thread.currentThread();
        String name = main.getName();//获取线程的名字
        System.out.println("主线程名称"+name);
        long id = main.getId();//获取线程唯一标识ID
        System.out.println("主线程ID"+id);
        int priority = main.getPriority();//获取线程优先级
        System.out.println("主线程优先级"+priority);
        boolean isLive = main.isAlive();//判断线程是否活着
        System.out.println("线程是否活着"+isLive);
        boolean isDemon = main.isDaemon();//判断线程是否为守护线程
        System.out.println("线程是否为守护线程"+isDemon);
        boolean isInterrupted = main.isInterrupted();//判断线程是否被中断
        System.out.println("线程是否被中断"+isInterrupted);
    }
}
