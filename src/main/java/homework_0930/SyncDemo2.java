package homework_0930;

/**
 * 同步块
 * 有效的缩小同步范围可以在保证并发安全的前提下尽可能的提高并发效率
 * 同步块语法
 * synchronized(同步监视对象){
 *     多个需要同步执行的代码片段
 * }
 */
public class SyncDemo2 {
    public static void main(String[] args) {
        Shop shop1 = new Shop();
        Shop shop2 = new Shop();
        Thread t1 = new Thread("男"){
            @Override
            public void run() {
                shop1.buy();
            }
        };
        Thread t2 = new Thread("女"){
            @Override
            public void run() {
                shop2.buy();
            }
        };
        t1.start();
        t2.start();
    }
}
class Shop{
    /*在成员方法上使用synchronized后,同步监视器对象不可选,就是this*/
    public void buy(){
//    public synchronized void buy(){
        try {
            Thread t = Thread.currentThread();
            System.out.println(t.getName()+":正在挑衣服");
            Thread.sleep(5500);
            /*同步监视器对象的选取
            * 1.必须是引用对象
            * 2:多个不同的代码片段看到的该对象是同一个
            * 3:存在并发问题*/
            synchronized (this){
                /*不可以,看到的不是同一对象*/
                /*字符串可以填入,却不适合,因为其他任务也是看到也是惟一的字符串变量,相当于所有都在同步块内,
                * 会降低效率*/
//            synchronized (new Object()){
                System.out.println(t.getName()+":正在试衣间试衣服");
                Thread.sleep(5500);
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
