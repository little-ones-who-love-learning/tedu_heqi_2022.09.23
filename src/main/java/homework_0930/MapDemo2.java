package homework_0930;

import java.util.*;

/**
 * Map的遍历
 * 1,遍历所有的key
 * 2,遍历所有的value
 * 3,遍历每一组键值对
 */
public class MapDemo2 {
    public static void main(String[] args) {
        Map<String,Integer> map = new HashMap<>();
        map.put("语文",106);
        map.put("数学",125);
        map.put("英文",112);
        map.put("物理",102);
        map.put("化学",95);
        map.put("生物",86);
        Set<String> keyset = map.keySet();
        for(String s:keyset){
            System.out.println("key"+s);
        }

        Set<Map.Entry<String,Integer>> entrySet = map.entrySet();
        for(Map.Entry<String,Integer> e:entrySet){
            String key = e.getKey();
            Integer value = e.getValue();
            System.out.println(key+":"+value);
        }

        /*Collection  map.values()*/
        Collection<Integer> values = map.values();
        for(Integer i:values){
            System.out.println(i);
        }
        /*forEach()方法*/
        values.forEach(i-> System.out.println(i));
        map.forEach((k,v)-> System.out.println(k+":"+v));
    }
}
