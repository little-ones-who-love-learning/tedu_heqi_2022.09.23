package homework_0930;

import java.util.Scanner;

/**
 * sleep阻塞
 * 线程提供了一个静态方法:
 * static void sleep(long ms)
 * 该方法可以让执行这个方法的线程进入阻塞状态(BLOCK),指定时间后,回到(RUNNABLE)
 */
public class SleepDemo {
    public static void main(String[] args) {
        System.out.println("程序开始了");
        Scanner scan = new Scanner(System.in);
        System.out.println("请输入一个数字:");
        for (int i = scan.nextInt(); i >0 ; i--) {
            System.out.println(i);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("程序结束了");
    }
}
