package homework_0930;

import java.util.HashMap;
import java.util.Map;

/**
 * Map 查找表 JAVA集合框架的一员
 * 接口 体现为一个多行两列的表格,左列为key,右列为value
 * 常见的实现类 java.util.HashMap
 * 散列算法,当今查询速度最快的数据结构
 */
public class MapDemo {
    public static void main(String[] args) {
        Map<String,Integer> map = new HashMap<>();
        /*map的key不可以重复*/
        /*如果返回null,说明这是一个新的key
        * 否则返回的是被修改的value*/
        Integer value = map.put("语文",106);
        System.out.println(value);
        value = map.put("语文",126);
        System.out.println(value);
        map.put("数学",106);
        map.put("英语",106);
        map.put("物理",106);
        map.put("化学",106);
        /*map也重写了toString()*/
        System.out.println(map);
        /*根据给定的key获取对应的value*/
        value = map.get("语文");
        System.out.println(value);
        /*获取map中键值对的个数*/
        int size = map.size();
        System.out.println(size);
        /*根据给定key删除键值对 ,返回值为这个被删去键值对的value*/
        value= map.remove("语文");
        System.out.println(value);
        boolean ck = map.containsKey("数学");
        boolean cv = map.containsValue(106);
        System.out.println(ck);
        System.out.println(cv);
        map.clear();
        System.out.println(map);
    }
}
