package homework_0930;

/**
 * 守护线程是通过线程的setDaemon(boolean on) 传入参数true将一个普通线程设置二转变为守护线程
 * 两者区别在于结束
 * Java中所有的普通线程退出,Java进程就会退出,同时杀死所有还在运行的守护线程
 *
 * 守护线程适合那些我们不关心结束时机的任务,只要主线程执行完毕他们也会跟着停下来就好
 * 最具代表就是GC(是不是会清理缓存)
 */
public class DaemoThreadDemo {
    public static void main(String[] args) {
        Thread rose = new Thread(){
            @Override
            public void run() {
                for (int i = 0; i < 5; i++) {
                    System.out.println("rose:let me go!");
                    try {
                        //线程的阻塞要捕获处理异常
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println("rose:啊啊啊");
                System.out.println("噗通");
            }
        };
        Thread jack = new Thread(){
            @Override
            public void run() {
                while(true){
                    System.out.println("jack:you jump i jump!");
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        rose.start();
        jack.setDaemon(true);
        jack.start();

    }
}
