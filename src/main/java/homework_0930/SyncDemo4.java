package homework_0930;

/**
 * 互斥锁
 * 当使用多个synchronized锁定多个代码片段时,并且指定 的同步器监视对象是同一个时,那么这些代码片段之间
 * 就是互斥的,多个线程不能同时执行他们.
 */
public class SyncDemo4 {
    public static void main(String[] args) {
        Boo b1 = new Boo();
        Boo b2 = new Boo();
        Thread t1 = new Thread(){
            @Override
            public void run() {
                b1.methodA();
            }
        };
        Thread t2 = new Thread(){
            @Override
            public void run() {
                b2.methodB();
            }
        };
        t1.start();
        t2.start();
    }
}
class Boo{
    public synchronized void methodA(){
        Thread t = Thread.currentThread();
        System.out.println(t.getName()+"正在执行方法a");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("a方法执行完毕");
    }
    public synchronized void methodB(){
        synchronized (this){
            Thread t = Thread.currentThread();
            System.out.println(t.getName()+"正在执行方法B");
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("B方法执行完毕");
        }
    }
}