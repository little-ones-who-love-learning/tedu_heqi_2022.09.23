package homework_0930;

/**
 * j静态方法上如果使用了synchronized,那么该方法一定具有同步效果*/
public class SyncDemo3 {
    public static void main(String[] args) {
        Foo f1 = new Foo();
        Foo f2 = new Foo();
        Thread t1 = new Thread(){
            @Override
            public void run() {
                f1.dosome();
            }
        };
        Thread t2 = new Thread(){
            @Override
            public void run() {
                f2.dosome();
            }
        };
        t1.start();
        t2.start();
    }
}
class Foo{
    /*如果这里将static去掉会如何
    * 此时dosome会变为一个成员方法,synchronized对应的锁就应该是this
    * 所以上面程序中还是可以同步执行该方法,没有解决并发问题
    *
    * static方法不同,该方法全局唯一
    * 静态方法锁的对象为当前类的类对象(Class的实例)*/
    public static void dosome(){
        synchronized (Foo.class){
            Thread t = Thread.currentThread();
            System.out.println(t.getName()+"正在执行dosome方法");
            try {
                Thread.sleep(5000);
                System.out.println(t.getName()+"方法执行完毕");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
