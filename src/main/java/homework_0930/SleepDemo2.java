package homework_0930;

/**
 * sleep方法必须处理中断异常.
 * 当线程调用sleep方法睡眠阻塞的时候,再调用线程的interrupt方法
 * sleep就会抛出异常
 */
public class SleepDemo2 {
    public static void main(String[] args) {
        Thread lin = new Thread("林永健"){
            @Override
            public void run() {
                System.out.println(getName()+"睡会觉吧");
                try {
                    Thread.sleep(100000);
                } catch (InterruptedException e) {
                    System.out.println("谁再吵");
                }
                System.out.println("醒了");
            }
        };
        Thread huang = new Thread(){
            @Override
            public void run() {
                System.out.println(getName()+":大锤80,小锤40");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("咣当");
                lin.interrupt();
            }
        };

        lin.start();
        huang.start();

    }
}
