package homework_0930;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 线程池
 */
public class ThreadPoolDemo {
    public static void main(String[] args) {
        /*创建一个线程池*/
        ExecutorService threadPool = Executors.newFixedThreadPool(10);
        /*指派任务*/
        for (int i = 0; i < 5; i++) {
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    Thread t = Thread.currentThread();
                    System.out.println(t.getName()+"正在执行一个任务");
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(t.getName()+"执行任务完毕");
                }
            };
            /*将一个任务交给了线程池*/
            threadPool.execute(runnable);
        }
        threadPool.shutdown();
        System.out.println("线程池停止了");
    }
}
