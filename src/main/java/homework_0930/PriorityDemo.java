package homework_0930;

/**
 * 线程优先级
 * 线程有10个优先级,分别用1-10表示,其中1最低,10最高,5是默认值
 * 当一个线程调用start后便纳入了线程调度器中被统一管理.线程只能被动的被分配时间片得以并发运行
 * 不能主动获取时间片.通过调整现成的优先级可以最大程度的改变获取时间片段的概率
 * 在同一个CPU核心中并发执行的线程中优先级最高的线程获取时间片的次数最多.
 */
public class PriorityDemo {
    public static void main(String[] args) {
        Thread min = new Thread() {
            @Override
            public void run() {
                for (int i = 0; i < 1000; i++) {
                    System.out.println("min");
                }
            }
        };
        Thread normal = new Thread() {
            @Override
            public void run() {
                for (int i = 0; i < 1000; i++) {
                    System.out.println("正常优先级");
                }
            }
        };
        Thread max = new Thread() {
            @Override
            public void run() {
                for (int i = 0; i < 1000; i++) {
                    System.out.println("max");
                }
            }
        };
        min.setPriority(Thread.MIN_PRIORITY);//Thread类中定义的常量属性
        max.setPriority(Thread.MAX_PRIORITY);
        min.start();
        normal.start();
        max.start();
    }
}
