package homework_0930;

/**
 * 线程并发安全问题
 * 当多个线程并发操作同一临界资源由于线程切换时间不确定,导致线程执行顺序出现混乱从而导致不良后果
 */
public class SyncDemo1 {
    public static void main(String[] args) {
        Table table = new Table();
        Thread t1 = new Thread(){
            @Override
            public void run() {
                while(true){
                    int bean  = table.getBeans();
                    Thread.yield();
                    System.out.println(getName()+":"+bean);
                }
            }
        };
        Thread t2 = new Thread(){
            @Override
            public void run() {
                while(true){
                    int bean  = table.getBeans();
                    Thread.yield();
                    System.out.println(getName()+":"+bean);
                }
            }
        };
        t1.start();
        t2.start();
    }
}
class Table{
    private int beans = 20;

    public synchronized int getBeans() {
        if(beans == 0){
            throw new RuntimeException("没有豆子了");
        }
        Thread.yield();//主动放弃本次时间片
        return beans--;
    }
}
