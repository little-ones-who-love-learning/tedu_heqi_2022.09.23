package homework_0926;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class CopyDemo2 {
    public static void main(String[] args) throws IOException {
        FileInputStream fis = new FileInputStream("./img.jpg");
        FileOutputStream fos = new FileOutputStream("./img_cp.jpg");
        /*注意,这里是字节数组,表明要读取8kb的内容*/
        byte[] data = new byte[1024*8];
        /*此方法的返回值是读取了多少位字节,若没有读取到,返回值为-1*/
        int d;
        /*系统的时间为一个long 型*/
        long start = System.currentTimeMillis();
        while((d=fis.read(data))!=-1){
            /*下面方法的参数需要注意,分别为字节数组,开始写入的字节数组下表,结束写入的字节数组下标*/
            fos.write(data,0,d);
        }
        long end = System.currentTimeMillis();
        System.out.println("图片复制耗时"+(end-start)+"ms");
    }
}
