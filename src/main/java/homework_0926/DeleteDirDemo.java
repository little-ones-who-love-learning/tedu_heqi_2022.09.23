package homework_0926;

import java.io.File;

public class DeleteDirDemo {
    public static void main(String[] args) {
        File dir = new File("./a");
        if (dir.exists()) {
            /*delete()在删除时必须是一个空目录*/
            dir.delete();
            System.out.println("该目录已删除");
        } else {
            System.out.println("该目录不存在");
        }
    }
}
