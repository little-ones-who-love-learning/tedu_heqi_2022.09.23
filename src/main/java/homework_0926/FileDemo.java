package homework_0926;

import java.io.File;

public class FileDemo {
    public static void main(String[] args) {
        File file = new File("./demo.1.2.3.txt");
        /*获取文件大小,单位是字节*/
        long len = file.length();
        /*获取文件的名字*/
        String name = file.getName();
        /*文件是否可读*/
        boolean cr = file.canRead();
        /*文件是否可写*/
        boolean cw = file.canWrite();
        /*文件是否隐藏*/
        boolean ih = file.isHidden();
    }
}
