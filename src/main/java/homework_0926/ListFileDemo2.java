package homework_0926;

import java.io.File;
import java.io.FileFilter;

public class ListFileDemo2 {
    public static void main(String[] args) {
        File dir = new File("./src/main/src/java/file");
        if(dir.isDirectory()){
            /*文件过滤器,内部类创建方式*/
            FileFilter fileFilter = new FileFilter() {
                @Override
                public boolean accept(File pathname) {
                    return pathname.getName().contains("f");
                }
            };
            /*文件过滤器作为此处的参数传入
            * 回调模式*/
            File[] subs = dir.listFiles(fileFilter);
            for(File sub:subs){
                System.out.println(sub.getName());
            }
        }
    }
}
