package homework_0926;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Test1 {
    public static void main(String[] args) throws IOException {
        Scanner scan = new Scanner(System.in);
        while (true) {
            System.out.println("请输入需要创建的文件名称");
            String fileName = scan.nextLine();
            File file = new File(fileName);
            if (file.exists()) {
                System.out.println("文件已存在");
            } else {
                file.createNewFile();
                System.out.println("文件已创建");
                break;
            }
        }
    }
}
