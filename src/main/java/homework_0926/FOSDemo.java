package homework_0926;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FOSDemo {
    public static void main(String[] args) throws IOException {
        /*此处需要抛出文件找不到的异常*/
        FileOutputStream fos = new FileOutputStream("./fos.dat");
        /*此处添加IO异常*/
        fos.write(1);
        fos.write(5);//结果是向文件中写入了两个字节
    }
}
