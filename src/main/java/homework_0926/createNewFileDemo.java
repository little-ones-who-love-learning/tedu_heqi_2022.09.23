package homework_0926;

import java.io.File;
import java.io.IOException;

public class createNewFileDemo {
    public static void main(String[] args) throws IOException {
        /*在相对路径中,默认就是从./开始的,可以省略*/
        File file = new File("./test.txt");
        /*如果文件不存在,会报java.io.Exception*/
        File file1 = new File("./a/test.txt");
        if(file.exists()){
            System.out.println("文件已存在");
        }else{
            /*createNewFile()在方法上抛出IO异常*/
            file.createNewFile();
            System.out.println("文件已创建");
        }
    }
}
