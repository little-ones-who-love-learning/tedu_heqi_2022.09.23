package homework_0926;

import java.io.File;
import java.io.IOException;

public class MkDirDemo {
    public static void main(String[] args) throws IOException {
        File dir = new File("./a/b/c//e/f");
        if (dir.exists()) {
            System.out.println("该目录已存在");
        } else {
            /*创建目录,抛出IO异常
            * mkDir()要求需创建的目录的目录必须存在
            * mkDirs()不需要,此方法会把所有的目录都创建出来,推荐此方法*/
            dir.mkdirs();
            System.out.println("目录已创建");
        }
    }
}
