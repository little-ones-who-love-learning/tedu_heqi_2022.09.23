package homework_0926;

import java.io.FileInputStream;
import java.io.IOException;

public class FISDemo {
    public static void main(String[] args) throws IOException {
        FileInputStream fis = new FileInputStream("./fos.dat");
        int d = fis.read();//输入流读取一个字节
        d = fis.read();//读取第二个字节
        /*如果读取到文件末尾,d=-1*/
        /*最后流使用完,应该关闭*/
        fis.close();
    }
}
