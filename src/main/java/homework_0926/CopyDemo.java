package homework_0926;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class CopyDemo {
    public static void main(String[] args) throws IOException {
        FileOutputStream fos = new FileOutputStream("img_cp.jpg");
        FileInputStream fis = new FileInputStream("img.jpg");
        int d;
        /*系统时间计时*/
        long start = System.currentTimeMillis();
        /*按字节循环读取,写入直到文件末尾*/
        while((d = fis.read())!=-1){
            fos.write(d);
        }
        long end = System.currentTimeMillis();
        System.out.println("图片复制耗时:"+(end-start)+"ms");
    }
}
