package homework_0926;

import java.io.File;
import java.util.Scanner;

public class Test3 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("请输入关键字");
        String key = scan.nextLine();
        File dir = new File(".");
        if(dir.isDirectory()){
            File[] subs = dir.listFiles();
            for(File sub:subs){
                if(sub.getName().contains(key)){
                    System.out.println(sub.getName());
                }
            }
        }
    }
}
