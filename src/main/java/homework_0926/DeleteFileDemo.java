package homework_0926;

import java.io.File;

public class DeleteFileDemo {
    public static void main(String[] args) {
        /*先定位文件位置*/
        File file = new File("test.txt");
        if (file.exists()) {
            file.delete();
            System.out.println("文件已删除");
        }else{
            System.out.println("文件不存在");
        }
    }
}
