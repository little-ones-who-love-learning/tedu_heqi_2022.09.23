package homework_0926;

import java.io.File;
import java.io.FileFilter;

public class Test2 {
    public static void main(String[] args) {
        File dir = new File(".");
        if (dir.isDirectory()) {
            /*文件过滤器的lambda表达式写法*/
            File[] subs = dir.listFiles(pathname -> pathname.getName().contains("f"));
            for (File sub : subs) {
                System.out.println(sub.getName());
            }
        }
    }
}
