package homework_0926;

import java.io.File;

public class ListFileDemo {
    public static void main(String[] args) {
        File file = new File(".");
        /*获取当前目录下的所有子项
         * 文件是一个目录才可以获取子项*/
        if (file.isDirectory()) {
            /*用一个file 数组来接收*/
            File[] subs = file.listFiles();
            /*增强for循环遍历这个文件数组*/
            for(File sub:subs){
                System.out.println(sub.getName());
            }
        }
    }
}
