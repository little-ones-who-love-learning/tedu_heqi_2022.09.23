package homework_0927;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;

public class OSWDemo {
    public static void main(String[] args) throws IOException {
        /*java.io.InputStream和java.io.OutputStream是所有字节输入流和字节输出流的超类*/
        /*java.io.Reader和java.io.Writer是所有字符输入流和字符输出流的超类*/
        FileOutputStream fos = new FileOutputStream("fos.txt",true);
        /*创建转换流的时候通常要明确字符集，因为可能存在跨平台问题*/
        OutputStreamWriter osw = new OutputStreamWriter(fos, StandardCharsets.UTF_8);
        osw.write("常常刺破云霞，与你一生牵挂");
        osw.close();
    }
}
