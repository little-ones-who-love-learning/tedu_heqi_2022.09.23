package homework_0927;

import java.io.*;

public class CopyDemo3 {
    public static void main(String[] args) throws IOException {
        FileInputStream fis = new FileInputStream("img.png");
        /*缓冲字节流可以加快文件的读写效率
         * 这里在创建流的时候加的第二个参数为：一次性读取的字节数量即8kb*/
        BufferedInputStream bis = new BufferedInputStream(fis,1024*8);
        FileOutputStream fos = new FileOutputStream("img_cp.png");
        BufferedOutputStream bos = new BufferedOutputStream(fos,1024*8);
        int d;
        while((d=bis.read())!= -1){
            bos.write(d);
        }
        bis.close();
        bos.close();
    }
}
