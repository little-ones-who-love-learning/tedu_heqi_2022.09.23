package homework_0927;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Arrays;

public class OOSDemo {
    public static void main(String[] args) throws IOException {
        String name = "刘桑";
        int age = 55 ;
        String gender = "男";
        String[] otherInfo = {"技术好","技术超好","人贼壮","身体倍儿好"};
        FileOutputStream fos = new FileOutputStream("user.dat");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        Person p = new Person(name,age,gender,otherInfo);
        oos.writeObject(p);
        oos.close();

    }
}
class Person{
    String name;
    int age;
    String gender;
    String[] otherInfo;

    public Person(String name, int age, String gender, String[] otherInfo) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.otherInfo = otherInfo;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", gender='" + gender + '\'' +
                ", otherInfo=" + Arrays.toString(otherInfo) +
                '}';
    }
}
