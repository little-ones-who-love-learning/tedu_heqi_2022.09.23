package homework_0927;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class WriteStringDemo {
    public static void main(String[] args) throws IOException {
        /*实现记事本的功能，即向文件中写入字符串
         * 文件流的创建，后面还可以传入一个参数，true为追加模式，false为覆盖模式，如果不写，默认为false*/
        FileOutputStream fos = new FileOutputStream("fos.txt");
        /*下面是两种写入方式*/
//        String line = "爱你孤身走暗巷，爱你不跪的模样";
//        byte[] data = line.getBytes(StandardCharsets.UTF_8);
//        fos.write(data);
        fos.write("他们说，要勇敢逆着光".getBytes(StandardCharsets.UTF_8));
        fos.close();
    }
}
