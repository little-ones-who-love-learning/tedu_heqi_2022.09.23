package homework_0927;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class PWDemo {
    public static void main(String[] args) throws  FileNotFoundException {
        PrintWriter pw = new PrintWriter("pw.txt");
        pw.println("让我在看你一眼,从南到北");
        pw.println("像是北五环路,蒙蔽的双眼");
        pw.close();
    }
}
