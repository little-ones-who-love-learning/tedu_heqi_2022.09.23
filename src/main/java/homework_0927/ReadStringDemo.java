package homework_0927;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class ReadStringDemo {
    public static void main(String[] args) throws IOException {
        File file = new File("./src/main/java/test/ReadStringDemo.java");
        long length = file.length();
        FileInputStream fis = new FileInputStream(file);
        byte[] data = new byte[(int)length];
        fis.read(data);
        /*将文件所有的数据一次性读入字节数组中，如何一次性将字节数组转化为字符串？*/
        String line = new String(data, StandardCharsets.UTF_8);
        System.out.println(line);
        fis.close();
    }
}
