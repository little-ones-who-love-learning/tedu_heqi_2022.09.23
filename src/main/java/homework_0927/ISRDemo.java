package homework_0927;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class ISRDemo {
    public static void main(String[] args) throws IOException {
        /*转换输入流读取文本数据*/
        FileInputStream fis = new FileInputStream("./src/main/java/test/PWDemo.java");
        /*字符流超类Reader提供了读取字符的方法，文件流是直接读取字节，InputStreamReader可以直接将字符读取为字节*/
        InputStreamReader isr = new InputStreamReader(fis);
        int d;
        while((d=isr.read())!= -1){
            /*因为将字符读取为了字节，要在控制台上输出，还要将读取的字节转换为字符*/
            System.out.print((char) d);
        }
        isr.close();
    }
}
